function loadJsAsync(t, e) {
  let n = document.createElement('script')
  n.type = 'text/javascript'
  n.src = t
  n.addEventListener(
    'load',
    function (t) {
      e(null, t)
    },
    false
  )
  let a = document.getElementsByTagName('head')[0]
  a.appendChild(n)
}

// Hàm để lấy domain dựa trên môi trường
const getDomainByEnv = () => {
  if (
    window.location.hostname.includes('testing') ||
    window.location.hostname.includes('internal-medpro.ddns.net') ||
    window.location.hostname.includes('localhost')
  ) {
    return 'https://bo-api-testing.medpro.com.vn'
  } else {
    return 'https://bo-api.medpro.com.vn:5000'
  }
}

// Hàm để tìm đối tác
const findPartner = () => {
  var xhr = new XMLHttpRequest()
  xhr.open(
    'GET',
    getDomainByEnv() +
      '/partner-domain/get-by-domain?domain=' +
      window.location.hostname,
    false
  )
  xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8')
  xhr.send()

  const partner = xhr.status === 200 ? JSON.parse(xhr.response) : null
  return partner
}

// Đối tượng đại diện cho localhost
const localhost = {
  config: {
    domain: 'PKH',
    domainId: 8870
  },
  name: 'hotfix',
  domain: ['localhost'],
  partnerId: 'medpro'
}

// Hàm để tìm đối tác dựa trên domain
const findPartnerByDomain = () => {
  const partner = findPartner()
  if (partner) {
    return partner
  } else {
    return localhost
  }
}

// Tìm thông tin đối tác
const partner = findPartnerByDomain()
console.log('partner :>> ', partner)

// Lấy đường dẫn pathname của trang hiện tại
const _pathname = window.location.pathname

// Kiểm tra và load chức năng chat nếu không có "-app" trong đường dẫn
if (!_pathname.includes('-app')) {
  if (partner) {
    if (partner.config) {
      // Khi DOM đã tải xong, thực hiện load script js và nhúng chat
      window.addEventListener(
        'DOMContentLoaded',
        function () {
          loadJsAsync(
            'https://webchat.caresoft.vn:8090/js/CsChat.js?v=4.0',
            function () {
              embedCsChat(partner.config)
            }
          )
        },
        false
      )
    }
  }
}

function embedCsChat(data) {
  var currentBodyPosition,
    globalData,
    hidePopup,
    hide_popup_global,
    isSPA,
    isMobile,
    csWidgetType,
    csWidgetPos,
    mouseDown,
    mouseUp,
    handleIframeMessage

  globalData = data
  const CS_LIVE_CHAT_DIVDRAG_DOM_ID = 'cs-div-drag' // Thêm khai báo const cho CS_LIVE_CHAT_DIVDRAG_DOM_ID

  var iframe = document.createElement('iframe')
  var div = document.createElement('div')
  var divDrag = document.createElement('div')
  var body = document.body
  var style = window.getComputedStyle(body, null)
  currentBodyPosition = style.position

  try {
    div.id = CS_LIVE_CHAT_DIVDRAG_DOM_ID // Sử dụng const khai báo cho CS_LIVE_CHAT_DIVDRAG_DOM_ID
    div.style.width = '300px'
    div.style.height = '30px'
    div.style.position = 'fixed'
    div.style.bottom = '0px'
    div.style.zIndex = 999999999
    div.style.overflow = 'hidden'
    div.style.margin = 0
    div.style.padding = 0
    div.style.border = 0
    div.style.background = 'transparent'

    divDrag.id = CS_LIVE_CHAT_DIVDRAG_DOM_ID // Sử dụng const khai báo cho CS_LIVE_CHAT_DIVDRAG_DOM_ID
    divDrag.style.width = '0px'
    divDrag.style.height = '0px'
    divDrag.style.position = 'absolute'
    divDrag.style.top = '0'
    divDrag.style.zIndex = 999
    divDrag.style.cursor = '-webkit-grab'

    iframe.id = CS_LIVE_CHAT_DIVDRAG_DOM_ID // Sử dụng const khai báo cho CS_LIVE_CHAT_DIVDRAG_DOM_ID
    // ...
    // Tiếp tục phần còn lại của mã của bạn
  } catch (e) {
    iframe.parentNode.removeChild(iframe)
    if (currentBodyPosition) {
      document.body.setAttribute('style', 'position:' + currentBodyPosition)
    } else {
      document.body.setAttribute('style', 'position:static')
    }
  }
}
